from rest_framework.views import APIView
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from django.core.files.storage import FileSystemStorage
from django.http import JsonResponse
from django.http import HttpResponse
import essentia.standard as sta
import os
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.clickjacking import xframe_options_exempt
from ratelimit.decorators import ratelimit
import random
import string

class FileUploadView(APIView):
    # parser_classes = (FileUploadParser,)
    @ratelimit(key='ip', method='POST', rate='1/h', block=True)
    @xframe_options_exempt
    @csrf_exempt
    def put(self, request, filename, format=None):
        file_obj = request.data['file']
        fs = FileSystemStorage()
        fileName = fs.save(filename, file_obj)

        path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/../media/' + fileName

        audio = sta.MonoLoader(filename=str(path))()
        key = sta.KeyExtractor()(audio)
        bpm = sta.RhythmExtractor2013()(audio)

        fs.delete(fileName)

        pitchNameClassMappings = {
            'C': 0,
            'C#': 1,
            'D': 2,
            'D#': 3,
            'E': 4,
            'F': 5,
            'F#': 6,
            'G': 7,
            'G#': 8,
            'A': 9,
            'A#': 10,
            'B': 11,
        }

        mode = 0 if key[1] == 'minor' else 1

        response = {'Key': pitchNameClassMappings[key[0]], 'Mode': mode, 'Confidence': key[2], 'BPM': bpm[0]}
        return JsonResponse(response)

    def rate(group, request):
        if request._request.environ['REMOTE_ADDR'] == '95.28.223.44':
            return '1000/h'
        return '100/h'

    @ratelimit(key='ip', method='POST', rate='1000/h', block=True)
    @xframe_options_exempt
    @csrf_exempt
    def post(self, request, format=None):
        file = request.FILES['file']
        if file.size > 78643200:
            return HttpResponse(status=413)
        fs = FileSystemStorage()
        fileName = fs.save(''.join(random.choice(string.ascii_uppercase) for _ in range(6)), file)

        path = os.path.dirname(os.path.dirname(os.path.abspath(__file__))) + '/../media/' + fileName

        audio = sta.MonoLoader(filename=str(path))()
        key = sta.KeyExtractor()(audio)
        bpm = sta.RhythmExtractor2013()(audio)

        keyString = key[0] + " " + key [1]

        fs.delete(fileName)

        pitchNameClassMappings = {
            'C': 0,
            'C#': 1,
            'D': 2,
            'D#': 3,
            'E': 4,
            'F': 5,
            'F#': 6,
            'G': 7,
            'G#': 8,
            'A': 9,
            'A#': 10,
            'B': 11,
        }

        isMinor = key[1] == 'minor'

        camMi = [ 5, 12, 7, 2, 9, 4, 11, 6, 1, 8, 3, 10 ]
        camMa = [ 8, 3, 10, 5, 12, 7, 2, 9, 4, 11, 6, 1 ]

        camN = str(camMi[pitchNameClassMappings[key[0]]] if isMinor else camMa[pitchNameClassMappings[key[0]]])
        camL = "A" if isMinor else "B"

        Camelot = camN + camL

        result = key + (bpm[0],)

        response = {'Name': file.name, 'Key': keyString, 'Camelot': Camelot, 'BPM': str(round(result[3]))}
        return JsonResponse(response)

